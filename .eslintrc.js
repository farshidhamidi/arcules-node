module.exports = {
  "env": {
    "browser": true,
    "node": true,
  },
  "extends": [
    "airbnb",
    "plugin:flowtype/recommended",
  ],
  "plugins": ["flowtype"],
  "parser": "babel-eslint",
};