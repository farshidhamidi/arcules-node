const Router = require('koa-router');
const router = new Router();
const config = require('../config');
const axios = require('axios');
const moment = require('moment');

router.get('/api/v1/weather/:lat/:lng', async (ctx) => {
  const { lat, lng } = ctx.params;
  // https://stackoverflow.com/questions/3895478/does-javascript-have-a-method-like-range-to-generate-an-array-based-on-suppl
  const lastWeekWeatherRequests = [...Array(7).keys()].map(day => {
    const time = moment().subtract(day, 'days').unix();
    return axios.get(
      `${process.env.DS_URL || config.dark_sky_api_url}${lat},${lng},${time}?exclude=,hourly,daily,flags`,
    );
  });
  const [
    { data: { currently: aDayAgo } },
    { data: { currently: twoDaysAgo } },
    { data: { currently: treeDaysAgo } },
    { data: { currently: fourDaysAgo } },
    { data: { currently: fiveDaysAgo } },
    { data: { currently: sixDaysAgo } },
    { data: { currently: sevenDaysAgo } },
  ] = await Promise.all(lastWeekWeatherRequests);

  ctx.body = {
    status: 'success',
    message: 'Successfully fetched weather data',
    data: [
      aDayAgo,
      twoDaysAgo,
      treeDaysAgo,
      fourDaysAgo,
      fiveDaysAgo,
      sixDaysAgo,
      sevenDaysAgo,
    ],
  };
});

module.exports = router;