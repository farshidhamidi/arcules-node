const Koa = require('koa');
const routes = require('./routes/index');
const cors = require('@koa/cors');

const app = new Koa();
const PORT = process.env.PORT || 3337;
app.use(async (ctx, next) => {
  ctx.set('Access-Control-Allow-Origin', '*');
  ctx.set('Access-Control-Allow-Methods', 'GET');
  ctx.set('Access-Control-Allow-Headers', 'Content-type');
  await next();
});
app.use(routes.routes());

const server = app.listen(PORT, () => {
  console.log(`Server listening on port: ${PORT}`);
});

module.exports = server;