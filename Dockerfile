FROM node:latest

COPY src/ /src
COPY package.json /src

ENV DS_URL=https://api.darksky.net/forecast/2de669c6b6445266cd87a8f70d12c95c/
ENV PORT=1337

RUN cd /src && \
    yarn install

CMD [ "node", "/src/index.js" ]